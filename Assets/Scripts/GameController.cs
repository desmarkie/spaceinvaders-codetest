﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	[Header("Player Object")]

	// Storing a ref to player object
	[SerializeField]
	[Tooltip("Main player object")]
	private PlayerController player;

	// Chucking main config vars in here. Would normally sit these in a separate object, but few enough here that it's ok

	// Acceleration applied to player
	[SerializeField]
	[Tooltip("Rate that movement increases")]
	private float _accelerationAmount = 0.1f;
	public float accelerationAmount {
		get { return _accelerationAmount; }
	}


	// Deceleration applied to player
	[SerializeField]
	[Tooltip("Rate that movement decreases")]
	private float _decelerationAmount = 0.98f;
	public float decelerationAmount {
		get { return _decelerationAmount; }
	}


	// Max speed for player
	[SerializeField]
	[Tooltip("Maximum speed for player")]
	private float _maxPlayerSpeed = 1f;
	public float maxPlayerSpeed {
		get { return _maxPlayerSpeed; }
	}



	[Header("Bullet Object")]

	[SerializeField]
	[Tooltip("Bullet prefab")]
	private GameObject bulletPrefab;

	[SerializeField]
	[Tooltip("Bullet acceleration")]
	private float _bulletAcceleration = 0.1f;
	public float bulletAcceleration {
		get { return _bulletAcceleration; }
	}

	[SerializeField]
	[Tooltip("Maximum bullet speed")]
	private float _maxBulletSpeed = 3f;
	public float maxBulletSpeed {
		get { return _maxBulletSpeed; }
	}

	[SerializeField]
	[Tooltip("Fire rate, higher values = slower rate")]
	private float minTimeBetweenShots = 0.1f;


	[Header("Enemy Object")]

	[SerializeField]
	[Tooltip("Enemy prefab")]
	private GameObject enemyPrefab;



	[Header("Game World Variables")]

	[SerializeField]
	[Tooltip("Min/Max X position for player")]
	private float _worldLimit = 10f;
	public float worldLimit {
		get { return _worldLimit; }
	}

	[SerializeField]
	[Tooltip("bullets are removed when past this Y value")]
	private float worldHeight = 10f;


	[Header("UI Controller")]

	[SerializeField]
	[Tooltip("Canvas controller object")]
	private CanvasController canvasController;



	// pooled bullet objects
	private List<GameObject> bullets = new List<GameObject>();
	private List<GameObject> deadBullets = new List<GameObject>();


	// input flags
	private bool leftPressed = false;
	private bool rightPressed = false;
	private bool firePressed = false;


	// fire rate timing
	private float shotTimeout = 0f;

	// lives left - todo, make this an arbitrary value, leaving as is for lazy UI add
	private int maxLives = 3;
	private int currentLives = 0;

	// playing?
	private bool gameStarted = false;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


		//Skip update if not playing
		if (!gameStarted) return;

		CheckInputs();


		// apply speed control to player
		float accel = 0f;
		if (leftPressed) accel -= _accelerationAmount;
		if (rightPressed) accel += _accelerationAmount;

		player.SetAcceleration(accel);


		// delay between shots if needed
		if (shotTimeout > 0f) shotTimeout -= Time.deltaTime;


		// shoot?
		if (firePressed && shotTimeout <= 0f)
		{
			// fire!
			FireShot();

			// set timeout
			shotTimeout = minTimeBetweenShots;
		}


		//disable dead bullets
		if (bullets.Count > 0)
			CheckForDeadBullets();
		
	}



	public void StartGame()
	{

		//init game, called from StartButton on StartPanel

		InitGameVars();

		canvasController.HideStartScreen();
		canvasController.ShowInGameUI();
		gameStarted = true;

	}

	public void RestartGame()
	{
		canvasController.HideEndScreen();
		canvasController.ShowStartScreen();
	}

	public void EndGame()
	{
		gameStarted = false;
		canvasController.HideInGameUI();
		canvasController.ShowEndScreen();
	}


	private void InitGameVars()
	{
		
		player.ResetPlayer();
		RemoveAllBullets();

		shotTimeout = 0f;
		currentLives = maxLives;
	}


	private void RemoveAllBullets()
	{

		while(bullets.Count > 0)
		{
			bullets[0].SetActive(false);
			deadBullets.Add(bullets[0]);
			bullets.RemoveAt(0);
		}
	}


	private void CheckForDeadBullets()
	{

		Vector3 pos;
		for (int i = bullets.Count-1; i >= 0; i--)
		{
			pos = bullets[i].transform.position;
			if( pos.y > worldHeight) //out of bounds, disable and pool
			{
				bullets[i].SetActive(false);
				deadBullets.Add(bullets[i]);
				bullets.RemoveAt(i);
			}
		}
	}

	private void FireShot()
	{

		//instantiate a bullet prefab
		GameObject bullet = GetBullet();
		bullet.GetComponent<BulletController>().ResetBullet();

		//set it's position
		bullet.transform.position = player.GetCannonTipPosition();

		//add it to pool + enable
		bullets.Add(bullet);
		bullet.SetActive( true );

	}

	//check for available dead bullets or instantiate a new one
	private GameObject GetBullet()
	{

		GameObject bullet;

		if( deadBullets.Count > 0 )
		{
			bullet = deadBullets[0];
			deadBullets.RemoveAt(0);
		}
		else
		{
			bullet = Instantiate(bulletPrefab);
			bullet.GetComponent<BulletController>().SetGameController( this );
		}

		return bullet;
	}


	private void CheckInputs()
	{

		// I'd normally break this out into another class and combine input checks for touch/keyboard/controller etc

		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			leftPressed = true;
		}
		else if (Input.GetKeyUp(KeyCode.LeftArrow))
		{
			leftPressed = false;
		}

		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			rightPressed = true;
		}
		else if (Input.GetKeyUp(KeyCode.RightArrow))
		{
			rightPressed = false;
		}

		if (Input.GetKeyDown(KeyCode.Space))
		{
			firePressed = true;
		}
		else if (Input.GetKeyUp(KeyCode.Space))
		{
			firePressed = false;
		}

	}
}
