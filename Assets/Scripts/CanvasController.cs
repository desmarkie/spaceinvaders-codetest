﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour {


	[SerializeField]
	private GameObject startPanel;

	[SerializeField]
	private GameObject endPanel;

	[SerializeField]
	private GameObject inGamePanel;

	[SerializeField]
	private GameObject life1;

	[SerializeField]
	private GameObject life2;

	[SerializeField]
	private GameObject life3;


	// Use this for initialization
	void Start () {

		startPanel.SetActive( true );
		endPanel.SetActive( false );
		inGamePanel.SetActive( false );

	}
	
	public void SetLives( int lives )
	{
		life1.SetActive( lives >= 1 );
		life2.SetActive( lives >= 2 );
		life3.SetActive( lives >= 3 );
	}


	public void ShowInGameUI()
	{
		inGamePanel.SetActive(true);
	}
	public void HideInGameUI()
	{
		inGamePanel.SetActive(false);
	}


	public void ShowStartScreen()
	{
		startPanel.SetActive(true);
	}
	public void HideStartScreen()
	{
		startPanel.SetActive(false);
	}


	public void ShowEndScreen()
	{
		endPanel.SetActive(true);
	}
	public void HideEndScreen()
	{
		endPanel.SetActive(false);
	}
}
