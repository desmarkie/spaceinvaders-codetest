﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

	private GameController gameController;

	private float velocity = 0f;

	public void ResetBullet()
	{
		velocity = 0f;
	}

	public void SetGameController( GameController gameController )
	{
		this.gameController = gameController;
	}
	
	// Update is called once per frame
	void Update () {

		velocity += gameController.bulletAcceleration;
		Mathf.Clamp(velocity, 0f, gameController.maxBulletSpeed);

		Vector3 pos = transform.position;
		pos.y += velocity;

		transform.position = pos;

	}
}
