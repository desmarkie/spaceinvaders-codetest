﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	// This is really sloppy I know! but for prototyping purposes it should give the designers enough
	// flexibilty before the relevant info gets stored in a permanent place somewhere else and applied on instantiation / init
	// Grabbing maxspeed/world limits for motion control
	[SerializeField]
	private GameController gameController;


	// Empty at tip of cannon, used for bullet positioning when firing
	[SerializeField]
	private GameObject cannonTip;


	// Current values for the player object
	private float acceleration = 0f;
	private float velocity = 0f;


	
	// Update is called once per frame
	void Update () {


		CalculateForces();
		ApplyForces();


	}

	public void ResetPlayer()
	{
		SetAcceleration(0f);
		SetVelocity(0f);
		Vector3 pos = transform.position;
		pos.x = 0f;
		transform.position = pos;
	}

	public Vector3 GetCannonTipPosition()
	{
		return cannonTip.transform.position;
	}


	// Methods used to control the player object, called from GameController
	public void SetAcceleration(float accel)
	{
		
		acceleration = accel;

	}

	// used to set velocity directly, e.g. when resetting
	public void SetVelocity(float vel)
	{
		
		velocity = vel;

	}




	// Player Motion values
	private void CalculateForces()
	{
		
		// apply acceleration 
		velocity += acceleration;

		// limit speed based on value in GameController
		Mathf.Clamp(
			velocity,
			-gameController.maxPlayerSpeed,
			gameController.maxPlayerSpeed
		);

		// decelerate
		velocity *= gameController.decelerationAmount;

		// round the velocity if it's small enough
		if (Mathf.Abs(velocity) <= 0.001f) velocity = 0f;

	}


	private void ApplyForces()
	{

		// add velocity to position ( could scale here if needed )
		Vector3 pos = transform.position;
		pos.x += velocity;

		// keep within game limits
		if( pos.x <= -gameController.worldLimit )
		{
			pos.x = -gameController.worldLimit;
			SetVelocity(0f);
		}
		else if( pos.x >= gameController.worldLimit )
		{
			pos.x = gameController.worldLimit;
			SetVelocity(0f);
		}

		// update transform
		transform.position = pos;

	}

}
